/**
 * \file move.c
 * \author Alexandre BARRAT
 * \date 2022-03-02
 * \version 1.0
 * \copyright CC-BY 4.0
 */

#include <msp430.h>
#include "move.h"


/**
 * \fn void init_pwm
 * \brief Initialize timer and set PWM for wheels motors.
 */
void init_pwm()
{
    /* Motors control pins
     *
     * P2.0 => Wheel A speed (INPUT)
     * P2.1 => Wheel A direction (OUTPUT)
     * P2.2 => Wheel A PWM (OUTPUT)
     * P2.3 => Wheel B speed (INPUT)
     * P2.4 => Wheel B PWM (OUTPUT)
     * P2.5 => Wheel B direction (OUTPUT)
     */
    // Motor A (right) PWM to TA1.1
    P2DIR |= BIT2;
    P2SEL |= BIT2;
    P2SEL2 &= ~BIT2;

    // Motor B (left) PWM to TA1.2
    P2DIR |= BIT4;
    P2SEL |= BIT4;
    P2SEL2 &= ~BIT4;

    // Set Timers PWM to motors PWM pins
    TA1CTL = TASSEL_2 | MC_1;               // source SMCLK pour TimerA , mode comptage Up
    TA1CCTL1 |= OUTMOD_7;                   // activation mode de sortie n�7 sur TA1.1
    TA1CCTL2 |= OUTMOD_7;                   // activation mode de sortie n�7 sur TA1.2
    TA1CCR0 = PWM_DELAY_MSP;                    // determine la periode du signal
    TA1CCR1 = (int) PWM_DELAY_MSP * 0;          // determine le rapport cyclique du signal TA1.1
    TA1CCR2 = (int) PWM_DELAY_MSP * 0;          // determine le rapport cyclique du signal TA1.2

    // Direction pin motor A (right)
    P2SEL &= ~BIT1;
    P2SEL2 &= ~BIT1;
    P2DIR |= BIT1;
    P2OUT &= ~BIT1;

    // Direction pin motor B (left)
    P2SEL &= ~BIT5;
    P2SEL2 &= ~BIT5;
    P2DIR |= BIT5;
    P2OUT |= BIT5;
}


/**
 * \fn void forward()
 * \brief The robot move forward.
 */
void forward()
{
    P2OUT &= ~BIT1;
    P2OUT |= BIT5;
    TA1CCR1 = (int) PWM_DELAY_MSP * MAX;  // determine le rapport cyclique du signal TA1.1 (right)
    TA1CCR2 = (int) PWM_DELAY_MSP * MAX;  // determine le rapport cyclique du signal TA1.2 (left)
}


/**
 * \fn void backward()
 * \brief The robot move backward.
 */
void backward()
{
    P2OUT |= BIT1;
    P2OUT &= ~BIT5;
    TA1CCR1 = (int) PWM_DELAY_MSP * MAX;  // determine le rapport cyclique du signal TA1.1 (right)
    TA1CCR2 = (int) PWM_DELAY_MSP * MAX;  // determine le rapport cyclique du signal TA1.2 (left)
}


/**
 * \fn void stop()
 * \brief Stop the robot.
 */
void stop()
{
    P2OUT &= ~BIT1;
    P2OUT |= BIT5;
    TA1CCR1 = (int) PWM_DELAY_MSP * 0;  // determine le rapport cyclique du signal TA1.1 (right)
    TA1CCR2 = (int) PWM_DELAY_MSP * 0;  // determine le rapport cyclique du signal TA1.2 (left)
}


/**
 * \fn void right()
 * \brief The robot turn right.
 */
void right()
{
    P2OUT &= ~BIT1;
    P2OUT &= ~BIT5;
    TA1CCR1 = (int) PWM_DELAY_MSP * MAX;  // determine le rapport cyclique du signal TA1.1 (right)
    TA1CCR2 = (int) PWM_DELAY_MSP * MAX;  // determine le rapport cyclique du signal TA1.2 (left)
}


/**
 * \fn void left()
 * \brief The robot turn left.
 */
void left()
{
    P2OUT |= BIT1;
    P2OUT |= BIT5;
    TA1CCR1 = (int) PWM_DELAY_MSP * MAX;  // determine le rapport cyclique du signal TA1.1 (right)
    TA1CCR2 = (int) PWM_DELAY_MSP * MAX;  // determine le rapport cyclique du signal TA1.2 (left)
}
