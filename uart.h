/**
 * \file uart.h
 * \author Alexandre BARRAT
 * \date 2022-02-09
 * \version 1.0
 */

#ifndef UART_H_
#define UART_H_

/* ratio = clock/baudrate = 10^6/9600 */
#define RATIO 104

/**
 * \fn void init_uart()
 * \brief Initialize UART communication.
 */
void init_uart();


/**
 * \fn void uart_send_char(unsigned char c)
 * \brief Send a char using UART communication.
 * \param c The char to send.
 */
void uart_send_char(unsigned char c);


/**
 * \fn void uart_send_string(char *string)
 * \brief Send a String and a new line using UART communication.
 * \param *string The String to send.
 */
void uart_send_string(char *string);

#endif /* UART_H_ */
