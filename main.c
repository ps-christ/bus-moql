/**
 * \file move.c
 * \author Alexandre BARRAT, Christian POUEDOGO
 * \date 2022-03-10
 * \version 1.0
 * \copyright CC-BY 4.0
 */


#include <msp430.h>
#include "move.h"
#include "uart.h"
#include "spi.h"
#include "msp430-arduino.h"

#define DIST_SENSOR 13
#define LED_RED   10
#define LED_GREEN 16
#define MANUAL 0x1111
#define AUTO   0x0000

/**
 * Mode de fonctionnement :
 */
int mode = MANUAL;
int d;

/**
 * Initialize timer A0 used by the delay function.
 */
void init_timer()
{
    // Init 1s timer
    TACCR0 = 50000;             // 63000 us = 63ms avec clock 1MHz
    TA0CTL |= MC_3;             // up and down (x2)
    TA0CTL |= TASSEL_2;         // Source SMLCK
    TA0CTL |= ID_3;             // Prediv to 8 (x8)
}

/**
 * Around time s delay
 */
void delay_timer(int time)
{
    int i;
    for (i=0; i<time; i++)
    {
        while (!(TA0CTL & TAIFG));  //attente flag TAIFG a 1
        TA0CTL &= ~TAIFG;           //RAZ TAIFG
    }
}


/**
 * main.c
 */
void main(void)
{
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer

    if (CALBC1_1MHZ == 0xFF || CALDCO_1MHZ == 0xFF)
    {
        __bis_SR_register(LPM4_bits); // Low Power Mode #trap on Error
    }
    else
    {
        // Factory parameters
        BCSCTL1 = CALBC1_1MHZ;
        DCOCTL = CALDCO_1MHZ;
    }
    // BCSCTL1= CALBC1_1MHZ;       //frequence d�horloge 1MHz
    // DCOCTL= CALDCO_1MHZ;        // "

    init_timer();
    init_pwm();
    init_spi();
    init_uart();
    uart_send_char('>');

    // LEDs
    pinMode(LED_RED, OUTPUT);
    pinMode(LED_GREEN, OUTPUT);

    // Distance sensor for emergency stop
    pinMode(DIST_SENSOR, INPUT);
    P1IE |= BIT3;
    P1IES &= ~BIT3;
    __enable_interrupt();

    while (1);
}



#pragma vector=PORT1_VECTOR
__interrupt void emergency_stop(void)
{
    stop();
    uart_send_string("obstacle !");
    uart_send_char('>');
    P1IFG &= ~BIT3;
}

/**
 * Receive char from UART and SPI. Act in function
 */
#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
    //---------------- UART
    if (IFG2 & UCA0RXIFG)
    {
        unsigned char c = UCA0RXBUF;

        switch (c)
        {
            case 'h':
                uart_send_char(c);
                uart_send_string("");
                uart_send_string("Available commands:");
                uart_send_string("    h    print this message");
                uart_send_string("    m    switch mode between manual and auto");
                uart_send_string("    z    the robot go forward (manual mode only)");
                uart_send_string("    s    the robot go backward (manual mode only)");
                uart_send_string("    q    the robot turn left (manual mode only)");
                uart_send_string("    d    the robot turn right (manual mode only)");
                uart_send_string("  space  the robot stops");
                uart_send_string("    i    send '1' to the MSP430G2231");
                uart_send_string("    o    send '0' to the MSP430G2231");
                uart_send_char('>');
                break;

            case 'z':
                if (mode == MANUAL) forward();
                break;

            case 'q':
                if (mode == MANUAL) left();
                break;

            case 'd':
                if (mode == MANUAL) right();
                break;

            case 's':
                if (mode == MANUAL) backward();
                break;

            case ' ':
                stop();
                break;

            case 'm':
                if (mode == AUTO)
                {
                    mode = MANUAL;
                    digitalWrite(LED_RED, LOW);
                }
                else
                {
                    mode = AUTO;
                    digitalWrite(LED_RED, HIGH);
                }
                break;

            case 'o':
                spi_send_char('0');
                uart_send_string("send 0");
                uart_send_char('>');
                break;

            case 'i':
                spi_send_char('1');
                uart_send_string("send 1");
                uart_send_char('>');
                break;

            default:
                uart_send_char(c);
                uart_send_string("");
                uart_send_string("Wrong command !");
                uart_send_char('>');
                break;
        }
    }

    //--------------- SPI
    else if (IFG2 & UCB0RXIFG)
    {
        while( (UCB0STAT & UCBUSY) && !(UCB0STAT & UCOE) );
        while(!(IFG2 & UCB0RXIFG));
        unsigned char c = UCB0RXBUF;
        uart_send_char(c);
        P1OUT ^= BIT6;
    }
}
