/**
 * \file move.h
 * \author Alexandre BARRAT
 * \date 2022-03-08
 * \version 1.0
 */

#ifndef MOVE_H_
#define MOVE_H_


#define PWM_DELAY_MSP 500
#define MAX 1
#define MIN 0


/**
 * \fn void init_pwm
 * \brief Initialize timer and set PWM for wheels motors.
 */
void init_pwm();

/**
 * \fn void forward()
 * \brief The robot move forward.
 */
void forward();

/**
 * \fn void backward()
 * \brief The robot move backward.
 */
void backward();

/**
 * \fn void stop()
 * \brief Stop the robot.
 */
void stop();

/**
 * \fn void right()
 * \brief The robot turn right.
 */
void right();

/**
 * \fn void left()
 * \brief The robot turn left.
 */
void left();

#endif /* MOVE_H_ */
