/**
 * \file uart.h
 * \author Alexandre BARRAT
 * \date 2022-02-09
 * \version 1.0
 * \copyright CC-BY 4.0
 */

#include <msp430.h>
#include "uart.h"


/**
 * Initialize UART communication.
 */
void init_uart()
{
    P1SEL  |= (BIT1 | BIT2);
    P1SEL2 |= (BIT1 | BIT2);

    UCA0CTL1 |= UCSWRST;    // Software reset
    UCA0CTL1 |= UCSSEL_3;   // SMCLK (2-3)

    UCA0CTL0 &= ~UCPEN;     // Disable parity
    UCA0CTL0 &= ~UCMSB;     // Set MSB first
    UCA0CTL0 &= ~UC7BIT;    // 8 data bits
    UCA0CTL0 &= ~UCSPB;     // 1 stop bit
    UCA0CTL0 &= ~UCMODE_3;  // set UART mode
    UCA0CTL0 &= ~UCSYNC;    // Asynchronous mode
    UCA0CTL1 &= ~UCDORM;    // No sleeping mode (all char set UCA0RXFG)

    UCA0CTL1 &= ~UCSWRST;   // End software reset

    UCA0BR0 = 104 & 0xFF;   // baudrate LSB
    UCA0BR1 = 104 >> 8;     // baudrate MSB
    UCA0MCTL = 10;

    IE2 |= UCA0RXIE;        // Enable USCI_A0 RX interrupt
    __bis_SR_register(GIE); // interrupts enabled
}


/**
 * Send a char using UART communication.
 */
void uart_send_char(unsigned char c)
{
    while (!(IFG2 & UCA0TXIFG));    // USCI_A0 TX buffer ready?
    UCA0TXBUF = c;                  // TX -> RXed character
}

/**
 * Send a String using UART communication.
 */
void uart_send_string(char *string)
{
    while (*string)
    {
        uart_send_char(*string++);
    }
    uart_send_char('\r');
    uart_send_char('\n');
}
