/**
 * \file spi.c
 * \author Alexandre BARRAT
 * \date 2022-03-02
 * \version 1.0
 * \copyright CC-BY 4.0
 */

#ifndef SPI_H_
#define SPI_H_

#define CS          BIT4
#define SCLK        BIT5
#define DATA_OUT    BIT6
#define DATA_IN     BIT7

/**
 * \fn void init_spi()
 * \brief Initialize SPI communication.
 */
void init_spi();


/**
 * \fn void spi_send_char(unsigned char c)
 * \brief Send a char using SPI communication.
 * \param c The char to send.
 */
void spi_send_char(unsigned char c);


#endif /* SPI_H_ */
